FROM node:14.16.0-alpine3.13

USER node

ENV PATH="/home/node/docker-pnpm/node_modules/bin:${PATH}"

COPY --chown=node:node package.json /home/node/docker-pnpm/package.json

WORKDIR /home/node/docker-pnpm
RUN /usr/local/bin/yarn \
    && /usr/local/bin/yarn cache clean

WORKDIR /project

ENTRYPOINT ["/home/node/docker-pnpm/node_modules/bin/pnpm"]

LABEL org.opencontainers.image.authors="Helge Johansen <helge@kubes.no>"