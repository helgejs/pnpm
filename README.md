# pnpm (STILL IN DEVELOPMENT)

pnpm Docker container

A  Node.js container with [pnpm](https://www.npmjs.com/package/pnpm) 
installed for direct use in [Gitlab CI Pipelines](https://docs.gitlab.com/ee/ci/).  

# Updates
Updates daily if there is a new version available.

# Usage
Example usage in `.gitlab-ci.yml`
```yaml
tbd
```
# Source
[helgejs\pnpm](https://hub.docker.com/r/helgejs/pnpm)
